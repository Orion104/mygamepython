import pygame

pygame.init()

display_width = 800
display_height = 600

display = pygame.display.set_mode((display_width, display_height))  # размер дисплея
pygame.display.set_caption("Game")  # имя окна

game = True
while game:  # пока цикл True окно не закроется 
    for event in pygame.event.get():  # проверка на нажатие клавишь
        if event.type == pygame.QUIT:  # событие при выходи
            game = False

    display.fill((0, 0, 255))  # окрашивание дисплея в голубой цвет
    pygame.display.update()  # обновление дисплея